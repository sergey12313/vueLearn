var path = require('path');
var webpack =require('webpack');

module.exports = {
    entry: ['./src/app.js'],
    output: {
        path: __dirname + '/build/js',
        filename: 'main.js',
        library: 'home',
    },
    module: {
      // avoid webpack trying to shim process
      noParse: /es6-promise\.js$/,
      loaders: [
        {
          test: /\.vue$/,
          loader: 'vue'
        },
        {
          test: /\.js$/,
          // excluding some local linked packages.
          // for normal use cases only node_modules is needed.
          exclude: /node_modules|vue\/dist|vue-router\/|vue-loader\/|vue-hot-reload-api\//,
          loader: 'babel'
        }
      ],



    },
    plugins:[
      new webpack.optimize.UglifyJsPlugin({
        compress:{
          warnings: false,
          drop_console:true,
          unsafe:true
        }
      }),

    ],
    babel: {
      presets: ['es2015'],
      plugins: ['transform-runtime']
    },

  watch:true,
  devtool: "source-map",
    devServer:{
      host:"localhost",
      port:8080,
      contentBase:__dirname+'/build'
    }

};
