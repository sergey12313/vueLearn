import Vue from 'vue';
import VueResource from 'vue-resource';
Vue.use(VueResource);

let vm = new Vue({
  ready(){
    this.$http.get('/phones.json').then((data)=>{this.users = data.data}).catch(console.log("erro"));


  },
  el: '#app',
  data:{
    order: 1,
    newTodo: '',
    message:'Hello Vue.js',
    users:[],
    todos: [
      { text: 'Learn JavaScript' },
      { text: 'Learn Vue.js' },
      { text: 'Build Something Awesomedsada' },
    ]
  },
  methods:{
    addTodo: function(){
      let text = this.newTodo.trim()
      if(text){
        this.todos.push({text: text});
        this.newTodo= '';
      }
    },

  removeTodo: function (index) {
    this.todos.splice(index,1);

    }
  },
});
